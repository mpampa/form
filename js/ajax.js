function request(url,id)
{
	$.ajax({
		type:"Post",
		url : url,
		aync: true,
		cache:false,
		success:function(data){
			
			$('#'+id).html(data);	
		}
		
	});
}

function post(url,form,id)
{
	
	var f = $('#'+form);
	var inputs = f.find("input, select, button, textarea");
	var data = f.serialize();
	
	inputs.prop("disable",true);

	$.ajax({
			url: url,
			type: "post",
			data: data,
			success:function(data){
					$('#'+id).html(data);
			}	
	});
	
}
