<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form extends CI_Controller {

		public function index()
		{
			ini_set( 'display_errors', true );
			error_reporting( E_ALL ); 
			$this->load->library('session');
			$this->load->library('form_validation');
			$this->load->helper('url');

			$this->form_validation->set_rules('nombre', 'Username', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('telefono', 'Telefono', 'required');

	
			if($this->form_validation->run() == FALSE)
			{
				$this->load->view('form');
			}
			else
			{
				//insert into registro
				
				$this->load->view('gracias');
			}
		}	
}
